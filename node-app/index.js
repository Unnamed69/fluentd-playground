require('dotenv').config()
const express = require('express');
const FluentClient = require('@fluent-org/logger').FluentClient;
const app = express();

// The 2nd argument can be omitted. Here is a default value for options.
const logger = new FluentClient('node-fluentd', {
  socket: {
    host: 'fluentd',
    port: 24224,
    timeout: 3000, // 3 seconds
  },
  onSocketError: (err) => {
    console.error("error!", err)
  }
});

app.get('/', function(request, response) {
  logger.emit('follow', {from: 'userA', to: 'userB'});
  response.send('Hello World!');
});
const port = process.env.PORT || 3000;
app.listen(port, function() {
  console.log("Listening on " + port);
});